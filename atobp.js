atobp = b64 => {
	const idx = [...'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='].reduce((a, v, i) => (a[v] = i, a), {});
    for(var code, data = '', i = 0, pos = b64.indexOf('='), padded = pos > -1, len = padded ? pos : b64.length;	i < len;)
		code = idx[b64[i++]] << 18 | idx[b64[i++]] << 12 | idx[b64[i++]] << 6 | idx[b64[i++]],
		code != 0 && (data += String.fromCharCode(255 & code >>> 16, 255 & code >>> 8, 255 & code));
    return padded && (data = data.slice(0, pos - b64.length)), data
};